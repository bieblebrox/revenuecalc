angular.module('EanRevCalc.services', ['ngResource']).factory('Data', ['$resource', 'DataManager', function($resource, DataManager){
    return $resource('resources/data.json',{}, {
        'getData' : {'Method' : 'GET', 'isArray' : false}
    }).getData(function(resp){
        DataManager.loadCares(resp.Cares);
    });
}])
.factory('DataManager', ['$filter', function($filter){
    return {
        waitCare: false,
        selectedCares : [],
        allCares: [],
        addCare : function(){
            if (this.waitCare != false){
                var careListing = this.waitCare;
                careListing.index = this.selectedCares.length;
                this.selectedCares.push(careListing);
            };
            this.waitCare = false;
        },
        getSelectedCares: function(){
            return this.selectedCares;
        },
        removeListing: function(listing){
            this.selectedCares.splice(this.selectedCares.indexOf(listing), 1);
        },
        loadCares: function(Cares){
            this.allCares = Cares;
        },
        findCareById: function(id){
            var results = $filter('filter')(this.allCares, {'id': id});
            return (results.length == 1) ? results[0] : false;
        },
        getTotalRevenue: function(){
            var Count = 0;

            for (var i = 0; i < this.selectedCares.length; i++){
                Count += this.selectedCares[i].pricing;
                if (this.selectedCares[i].baseCare != false)
                    Count += (this.selectedCares[i].detail.length > 2) ? 4.05 : 2.95;
            }

            return Math.round(Count * 100) / 100;
        },
        reset: function(){
            this.selectedCares.splice(0, this.selectedCares.length);
        },
        checkoutCare: function(care, pricing, detail){
            var careListing = {
                pricing: pricing,
                care: care,
                detail: detail
            };
            this.waitCare = careListing;
        },
        addBaseCare: function(x){
            this.waitCare.baseCare = x;
        }
    };
}])